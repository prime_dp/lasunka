$(document).ready(function () {
    $('#fullpage').fullpage({
        anchors: ['main-section', 'products-catalog-section', 'product-1', 'product-2', 'product-3', 'product-4', 'product-5', 'product-6'],
        verticalCentered: false,
        'css3': true,
        'navigation': true,
        'navigationPosition': 'right',
        scrollBar: true,
        normalScrollElements: '.enable-scroll'
        //autoScrolling: false
        //dragAndMove: true
    });

    $(".main").mousemove(function(e) {
        parallaxIt(e, ".product-parallax-bg", 40);
        //parallaxIt(e, "img", -30);
    });

    function parallaxIt(e, target, movement) {
        var $this = $(".product-parallax-bg");
        var relX = e.pageX - $this.offset().left;
        //var relY = e.pageY - $this.offset().top;

        TweenMax.to(target, 1, {
            x: (relX - $this.width() / 2) / $this.width() * movement,
            //y: (relY - $this.height() / 2) / $this.height() * 20
        });
    }


    $(".products-catalog-slider").owlCarousel({
        items: 1,
        loop: true,
        dots: true,
        nav: false,
        autoplay: false
    });

    let $productSlider = $(".products-catalog-slider");

    $(window).resize(function() {
        showProductSlider();
    });

    function showProductSlider() {
        if ($productSlider.data("owlCarousel") !== "undefined") {
            if ($(window).width() < 769) {
                initialProductSlider();
            } else {
                destroyProductSlider();
            }
        }
    }
    showProductSlider();

    function initialProductSlider() {
        $productSlider.addClass("owl-carousel").owlCarousel({
            items: 1,
            loop: true,
            dots: true,
            nav: false,
            autoplay: false
        });
    }

    function destroyProductSlider() {
        $productSlider.trigger("destroy.owl.carousel").removeClass("owl-carousel");
    }



    $(window).on("load", function () {
        if ($(window).width() > 768) {
            destroyProductSlider();

        }
    });

    if ($(window).width() < 993) {
        let wow = new WOW(
            {
                boxClass: 'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset: 0,          // distance to the element when triggering the animation (default is 0)
            }
        );
        wow.init();
    }
});



